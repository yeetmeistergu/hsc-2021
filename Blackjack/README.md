# HSC Blackjack

The repository for my HSC Blackjack task. The player (you) is the hand with the yellow cards, and the dealer is the one at the top with the white cards.

## Problem Definition

Australians love gambling. So much so that over 80% of Australians engage in some kind of gambling, the highest rate in the world. One of the most popular forms of gambling in casinos is a card game called Blackjack. This is a game where the main opponent to the player is the dealer, and the other players do not affect an individual's chances at winning.

In an attempt to control the losses of Australians to casinos, we have been tasked to create a low-budget, lightweight, and unique version of the famous card game. The game will utilise a virtual currency, allowing an almost risk free implementation of the game which is still enjoyable to everyone. This also allows younger children to experience the thrill of gambling without falling into a life crippling addiction.

In addition, current implementations of the game attempt to emulate the casino "look", resulting in a boring, mundane, and redundant user interface which is irrelevant in this day and age.

To effectively implement this game, we will require a defined set of rules (normal casino rules are fine). In addition, we will need to clearly identify the needs and objectives of the task, as well as plan out the project using flowcharts and diagrams to map out the data flow and structure of the game. A feasibility report should be conducted to ensure that our software is feasible. Our software should utilise object-oriented programming to ensure maximum efficiency in the writing, maintenance, and usage of the software.

## Needs and Objectives

### Needs

- Due to the complexity of the rules in the game itself, this program MUST utilise and object-oriented design. This modular design allows for an easier development and maintenance process.
- As we want to encourage people to stick with this version of the game instead of visiting a casino, this program should be bug-free (or at the very least, devoid of any bugs which impact the user experience negatively in a great way).
- Adequate planning should be done in order to ensure the development period runs smoothly. These should include a DFD, System Flow Chart, and/or Feasibility report. This will help with the understanding of the structure, and function of modules required in the program.
- A decent understanding of programming and/or proficiency in a language is required. (In this case, the program will be built in Python)
- A coherent graphical user interface is needed as this program is intended for a general audience, which may include people of different needs and intellect. As such, the UI should be explicit and clear.
- Components required in this program:
  - Graphical user interface
    - Simple and Understandable
    - Unique, and engaging
    - Is ergonomic
  - An AI component with:
    - A consistent ruleset which is followed
    - Other AI players for the atmosphere
    - A dealer to deal the cards
    - Customisation options (how many other players, what is the threshold for the AI/Dealer, etc.)
  - General game functions:
    - Hitting
    - Standing
    - Betting
    - Splitting
    - A game which can be repeated (i.e. the program does not need to be restarted to start a new game)
    - Some kind of in game currency which DOES NOT have any real monetary value.
    - A card deck (or decks) which can be shuffled
    - The player has some method of winning, and will return some kind of reward in game.
    - Additional features such as forfeits, insurance, and doubling; as well as any other house rules *may* be considered, though not critical.

### Objectives

- The main objective of this game is to create a unique Blackjack experience which contains the thrills of gambling in a casino, but in a package which allows a user to quickly boot up, and enjoy their does of dopamine without the risk of financial ruin. This game should be functional, and without irritating bugs which may incentivise users to go back to the casinos. In addition, an intuitive interface that is easy to understand and navigate for all users is required. If all the features above are implemented successfully, this program will achieve the primary objective of the game.

### Boundaries

- Technical boundaries
  - Technological resources - As this program is for the general populace of users, this game needs to be functional on a wide range of differently spec'd hardware and as such, needs to be lightweight and as efficient as possible.
  - Technical ability - As I am a programming student, the extent of my programming capabilities and knowledge imposes an unfortunate boundary to this project.
  - Time restraints - As this project has a deadline, the amount of features, and the complexity of features will need to be considered in order for the program to be delivered within these boundaries.
  - Bugs - Due to time and coding efficiency, bugs are inevitable. Therefore, we must attempt to squash as many of them as possible (all of them ideally), but they still impose an annoying boundary if left unchecked.
- Social and ethical boundaries
  - Inclusivity and ergonomics - This program should be usable by people from all walks of lives, and of all abilities. The interface should be simple and understandable, and colour combinations and graphical choices should accommodate for any vision conditions.
  - Difficulty should be adjustable - As this is a casual game, it should be targeted towards players of all abilities. Including adjustable difficulty would be the most efficient way to ensure this, allowing us to keep players of all abilities engaged.
  - Currency should be virtual - This game should simulate the casino experience, whilst remaining low risk. As such, by having virtual rewards, we incentivise players to keep "chasing the thrill", without imposing any financial risk to them, thus achieving our goal of keeping people out of casinos.

## Feasibility Report

### Financial/Economic

This is not intended to be sold or otherwise generate income. Whilst no income is generated, as I am the sole developer and developing this program as part of my HSC, there are no real costs involved in the development of this program. There are also no costs associated with the distribution of this product, as it will be hosted on Bitbucket, which is free.

### Technical Feasibility

This program is extremely lightweight, and in the grand scheme of things, the required functions are not of extreme technical difficulty. The rules of the game are very simple, and there are not any overly complex mechanisms, the most difficult of which probably being the split function. As such, the development of this program will most likely not require any extremely advanced technical knowledge.

In addition, due to the lightweight nature of the program, only general hardware is needed. The program does not have any specific system requirements, aside from the required runtimes and modules, which are for the most part platform agnostic. As such, this program most likely can be developed and run on any system.

### Operational Feasibility

As stated above, the requirements for this program to be developed and run are extremely low. Being a simple game, this game should be useable by people of all levels of intellect and technical proficiency provided that:

- There is a functional and explicit user interface developed properly as defined in the needs and objectives.
- The program functions as intended, with the rules and functions working properly, and with no experience hindering bugs.
- The other needs and objectives specified are met.

### Schedule Feasibility

This project has a timeframe of 7 weeks. Being a project of a relatively small scale, and of relatively little complexity, this project should be attainable within the timeframe. In addition, the clear requirements and boundaries of this project, should assist with the development, and would allow for a very efficient development process. Additional time management techniques such as Gantt charts will further accelerate the process. The object-oriented approach which will be undertaken for this project will allow for numerous modules to be developed at once, further accelerating the development of this project.

**Verdict:**

Through the above assessments of the multiple criterions for feasibility, it can be determined that this project is indeed feasible and can be realistically attained.

## Systems Flowchart and Data Flow Diagram

![Flow](/Blackjack/assets/Flow.png)
![DFD](/Blackjack/assets/DFD.png)

A PDF version of both charts are available in the repository.

## Implementation methods

### Direct Cutover

A direct transfer to the new system, with the old system becoming inaccessible at the cut-off date. This method is also commonly used for the implementation of new systems.

**Advantages:**

- Fast switching between the new and existing (if any) system.
- Cheap (Little management of systems during the switchover is required).

**Disadvantages:**

- Requires the replacement system to be fully functional and integrated properly.
- Extremely risky (Data must be converted and switched over immediately. High risk of data loss should something be not fully functional).
- Stressful for all parties involved.
- Adequate preparation and training is required for users to quickly adapt to the new system to maintain productivity.

### Parallel Implementation

The new system is fully implemented, but a grace period is instilled where the old system is fully functional and accessible to allow for a smooth transition to the new system.

**Advantages:**

- Allows for users to pace their own transitions, and to assimilate and familiarise themselves to the new workflow.
- Gives developers time to identify any issues with the new system before production fully runs off of it.
- Gives time for data to be safely migrated.

**Disadvantages:**

- Management costs associated with running 2 systems.
- Data must be kept consistent across both systems to avoid potential conflicts and inconsistencies.
- Does not really incentivise users to familiarise and switch quickly to the new system.

### Phased Implementation

An amalgamation of the direct and parallel approaches, this implementation involves the transition of individual components of the system, with each component in the old system being replaced until the ultimately only the new system is left complete and running.

**Advantages:**

- The rate of change is steady, allowing users time to adapt.
- Slowly forces users out of the old system.
- No risk of duplicate/inconsistent data as a result of 2 systems running simultaneously.
- Allows more time for individual components to be developed, as not all components need to be fully functional when the transition begins.
- Allows for individual modules to be implemented at the least disruptive times possible (i.e. a new financial module implemented after EOFY).

**Disadvantages:**

- Longer period of stress for administrators and maintainers.
- Costs are equivalent to, if not more than that of the parallel approach.
- Data still needs to be kept consistent across both systems.
- May introduce more downtime for users as individual components are replaced.

### Pilot Implementation

Introduces the new system to a small userbase as a trial in order to receive feedback, and monitor the stability of the new system before the complete rollout of the new system.

**Advantages:**

- Provides a transition period similar to the previous 2 implementations.
- Any issues/instabilities will only affect a small userbase, reducing the risk of implementation.
- Allows for issues to be quickly isolated and fixed due to the small userbase.

**Disadvantages:**

- Costs are equivalent to, if not more than that of the parallel approach.
- Only effective with large user bases as it is otherwise pretty redundant.
- Data still needs to be kept consistent across both systems.
- Could cause workflow issues especially if users on both systems are required to interact with each other and/or each other's data.

**Verdict:**

Due to the small scale nature of this project, and the low budget and lack of a pre-existing system, the direct approach is the most feasible as since there are no risks associated with transferring data over from an old system, this method is the quickest and most efficient.
