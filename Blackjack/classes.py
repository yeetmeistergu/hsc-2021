import random

class Card():
    def __init__(self, v, s):
        faces = ["A","2","3","4","5","6","7","8","9","10","J","Q","K"]
        values = [11,2,3,4,5,6,7,8,9,10,10,10,10]
        # For testing purposes
        # faces = ["1","1","1","1","1","1","1","2","3","1","2","3","1"]
        # values = [11,10,11,10,11,10,11,10,11,10,11,10,11]
        suits = ["D","H","C","S"]
        self.value = values[v]
        self.suit = suits[s]
        self.face = faces[v]
        if self.face == "A":
            self.isAce = True
        else:
            self.isAce = False

    def getValue(self):
        return(self.value, self.isAce)

class Deck():
    def __init__(self, s):
        self.cards = []
        self.stacks = s
        self.generate()
    
    def generate(self):
        self.cards.clear()
        # For every deck in the stack of decks
        for i in range(self.stacks):
            # For every suit in the deck
            for l in range(4):
                # For every card
                for c in range(13):
                    self.cards.append(Card(c, l))

    def shuffle(self, c):
        for i in range(c):
            x = random.randint(0, len(self.cards)-1)
            y = random.randint(0, len(self.cards)-1)

            # P Howse's card switch algo
            temp = self.cards[x]
            self.cards[x] = self.cards[y]
            self.cards[y] = temp

class Hand():
    def __init__(self):
        self.cards = []
        self.bet = 0
        self.isBust = False
        self.isStood = False
    
    def add(self, c):
        self.cards.append(c)

    def clear(self):
        self.cards.clear()

    def getValue(self):
        handValue = 0
        hasAce = False

        for c in self.cards:
            handValue += c.value
            if c.isAce == True:
                hasAce = True

        if handValue > 21 and hasAce == True:
            for cd in self.cards:
                if handValue > 21:
                    if cd.isAce == True:
                        handValue -= 10

        if handValue > 21:
            self.isBust = True

        return handValue
    
    def getBust(self):
        self.getValue()
        if self.isBust:
            return True
        else:
            return False

    def doneDeal(self):
        if self.isStood or self.isBust or self.getValue == 21 or len(self.cards) == 5:
            return True
        else:
            return False

class Player():
    def __init__(self):
        self.hand = Hand()
        self.hand2 = Hand()
