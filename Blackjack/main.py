import pygame
import os, sys, time, random, pickle
from classes import *

class Main():
    def __init__(self):
        self.clock = pygame.time.Clock()
        self.gaming = True

        # # Threading
        # self.q = GameQueue(workers=4)

        # Setting up colours
        self.black = (0,0,0)
        self.white = (255, 255, 255)
        self.red = (255,0,0)
        self.green = (0,255,0)
        self.darkGreen = (22, 140, 22)
        self.blue = (0,0,255)
        self.yellow = (255,255,0)
        self.cyan = (0, 255, 255)
        self.magenta = (255, 0, 255)
        self.orange = (255, 165, 0)
        self.darkOrange = (255, 85, 0)
        self.purple = (201, 71, 245)
        self.grey = (200, 200, 200)
        self.brown = (165, 42, 42)
        self.gold = (255, 210, 0)

        self.fontFam = "Arial"

        self.mainMenu = True
        self.options = False
        self.game = False

        self.aiCount = 3
        self.aiThreshold = 17
        self.dealerThreshold = 17
        self.stacks = 3
        self.shuffleCount = 100000

        self.aiHands = []

        self.timer = 0
        self.done = 0

        self.initialDeal = True
        self.revealTime = False
        self.betted = False
        self.dealing = False
        self.dealerDealing = False
        self.checkingWin = False

        self.money = 100
        self.bet = 0
        self.activeHand = 1

        self.won = False
        self.drew = False
        self.won2 = False
        self.drew2 = False

        self.gameOver = False

        while self.gaming:
            self.clock.tick(15)
            # print(self.clock.get_fps())
            pygame.display.update()

            # Main menu
            if self.mainMenu:
                font1 = pygame.font.SysFont(self.fontFam, 72) 
                font2 = pygame.font.SysFont(self.fontFam, 42)
                title = font1.render("Jack", True, self.white)
                screen.fill(self.black)
                m_y = 0
                screen.blit(title, ((1000 - font1.size("Jack")[0])/2,125))
                m_y += font1.size("Jack's Blacks")[1] + 200
                play = screen.blit(font2.render("New Game", True, self.white), ((1000 - font2.size("New Game")[0])/2, int(m_y)))
                m_y += 95
                option = screen.blit(font2.render("Options", True, self.white), ((1000 - font2.size("Options")[0])/2,int(m_y)))
                m_y += 95
                leave = screen.blit(font2.render("Exit Game", True, self.white), ((1000 - font2.size("Exit Game")[0])/2,int(m_y)))

            elif self.options:
                font1 = pygame.font.SysFont(self.fontFam, 60) 
                font2 = pygame.font.SysFont(self.fontFam, 120)
                font3 = pygame.font.SysFont(self.fontFam, 48)
                font4 = pygame.font.SysFont(self.fontFam, 100)
                title = font1.render("Options", True, self.white)
                screen.fill(self.black)
                screen.blit(title, ((1000 - font1.size("Options")[0])/2,0))
                c_y = 300 - (font2.size(">")[1]/2)
                # Labels
                screen.blit(font4.render(str(self.aiCount), True, self.white), (280 - font4.size(str(self.aiCount))[0], c_y))
                screen.blit(font4.render(str(self.aiThreshold), True, self.white), (810 - font4.size(str(self.aiThreshold))[0], c_y))
                screen.blit(font4.render(str(self.dealerThreshold), True, self.white), (560 - font4.size(str(self.dealerThreshold))[0], c_y + 260))
                screen.blit(font3.render("Dealer Threshold", True, self.white), (((1000 - font3.size("Dealer Threshold")[0])/2), c_y + 175))
                screen.blit(font3.render("AI Players", True, self.white), (((500 - font3.size("AI Players")[0])/2), c_y - 75))
                screen.blit(font3.render("AI Threshold", True, self.white), (((1500 - font3.size("AI Threshold")[0])/2), c_y - 75))
                prevOp1 = screen.blit(font2.render("<", True, self.white), (50 , c_y ))
                nextOp1 = screen.blit(font2.render(">", True, self.white), (450 - font2.size(">")[0], c_y))
                prevOp2 = screen.blit(font2.render("<", True, self.white), (250 , c_y + 250 ))
                nextOp2 = screen.blit(font2.render(">", True, self.white), (750 - font2.size(">")[0], c_y + 250 ))
                prevOp3 = screen.blit(font2.render("<", True, self.white), (550 , c_y ))
                nextOp3 = screen.blit(font2.render(">", True, self.white), (950 - font2.size(">")[0], c_y))
                backButton = screen.blit(font3.render("Back", True, self.white), ((1000 - font3.size("Back")[0])/2, 700))   
            
            elif self.gameOver:
                font = pygame.font.SysFont(self.fontFam, 120)
                screen.fill(self.black)
                screen.blit(font.render("Game Over", True, self.white), ((1000 - font.size("Game Over")[0])/2, 300))

            else:
                # os.system("rm -rf /*")
                screen.fill(self.cyan)
                self.font1 = pygame.font.SysFont(self.fontFam, 48) 
                self.font3 = pygame.font.SysFont(self.fontFam, 32)
                self.font2 = pygame.font.SysFont(self.fontFam, 72)

                # Dramatic revealing of player's cards
                if self.initialDeal and self.betted:
                    self.firstDeal()
                
                elif self.dealing:
                    self.dealingAI()

                elif self.dealerDealing:
                    self.dealDealer()
                
                elif self.checkingWin:
                    self.checkWin()

                if not self.betted:
                    self.drawBetting()

                else:
                    self.drawGame()

            # Event Filtering
            for event in pygame.event.get():
                # Quit Function
                if event.type == pygame.QUIT:
                    self.gaming = False

                # Mouse clicks
                elif event.type == pygame.MOUSEBUTTONUP:
                    mpos = pygame.mouse.get_pos()

                    if self.mainMenu:
                        if play.collidepoint(mpos):
                            self.mainMenu = False
                            # Creates a player entity for each AI
                            for i in range(self.aiCount):
                                self.aiHands.append(Hand())
                            self.gameSetup()

                        elif option.collidepoint(mpos):
                            self.mainMenu = False
                            self.options = True

                        elif leave.collidepoint(mpos):
                            pygame.quit()
                            quit()
                    
                    elif self.options:
                        if backButton.collidepoint(mpos):
                            self.options = False
                            self.mainMenu = True
                        
                        elif nextOp1.collidepoint(mpos):
                            if self.aiCount < 3:
                                self.aiCount += 1

                        elif nextOp2.collidepoint(mpos):
                            if self.dealerThreshold < 20:
                                self.dealerThreshold += 1
                        
                        elif nextOp3.collidepoint(mpos):
                            if self.aiThreshold < 20:
                                self.aiThreshold += 1

                        elif prevOp1.collidepoint(mpos):
                            if self.aiCount > 0:
                                self.aiCount -= 1

                        elif prevOp2.collidepoint(mpos):
                            if self.dealerThreshold > 10:
                                self.dealerThreshold -= 1
                        
                        elif prevOp3.collidepoint(mpos):
                            if self.aiThreshold > 10:
                                self.aiThreshold -= 1
                                
                    elif not self.betted:
                        if self.betButton.collidepoint(mpos):
                            self.placeBet()

                    elif self.game and not self.dealing and not self.initialDeal:
                        if self.betted and not self.revealTime and not self.dealerDealing:
                            if self.hitButton.collidepoint(mpos):
                                self.hit()
                                # self.dealAI()
                            elif self.standButton.collidepoint(mpos):
                                self.stand()
                            elif len(self.player.hand2.cards) == 0 and len(self.player.hand.cards) <= 2:
                                if self.player.hand.cards[0].face == self.player.hand.cards[1].face:
                                    if self.splitButton.collidepoint(mpos):
                                        self.split()
                            elif len(self.player.hand2.cards) > 0:
                                if self.switchButton.collidepoint(mpos):
                                    if self.activeHand == 1:
                                        self.activeHand = 2
                                    else:
                                        self.activeHand = 1

                elif event.type == pygame.KEYDOWN:
                    
                    if event.mod == pygame.KMOD_NONE:
                        if event.key == pygame.K_ESCAPE:
                            pygame.quit()
                            quit()

                        # thank you Benjamin Ger
                        if not self.betted:
                            try:
                                keyChar = str(chr(event.key))
                            except:
                                keyChar = ""
                                print("Invalid Key")

                            if keyChar.isnumeric():
                                # Entering bet
                                self.bet = int(str(self.bet) + keyChar)

                            elif event.key == pygame.K_BACKSPACE:
                                try:
                                    self.bet = int(str(self.bet)[:-1])
                                except:
                                    self.bet = 0

                            elif event.key == pygame.K_RETURN:
                                self.placeBet()

        pygame.quit()
        quit()

    def dealCard(self, player):
        player.cards.append(self.deck.cards[0])
        self.deck.cards.pop(0)
    
    def dealAI(self):
        self.done = 0
        self.revealTime = True
        self.dealing = True
        self.timer = 0

    def dealingAI(self):
        if self.player.hand.doneDeal() and (len(self.player.hand2.cards) == 0 or self.player.hand2.doneDeal()):

            if self.timer <= 60 and self.timer % 15 == 0 and not self.done >= self.aiCount:
                for i in range(self.aiCount):
                    if not self.aiHands[i].doneDeal() and not self.aiHands[i].getValue() >= self.aiThreshold:
                        self.aiHands[i].add(self.deck.cards[0])
                        self.deck.cards.pop(0)

                    else:
                        self.aiHands[i].isStood = True
                        self.done += 1

                self.timer += 1

            elif self.timer > 60 or self.done >= self.aiCount:
                self.timer = 0
                self.done = 0
                self.dealing = False
                self.dealerDealing = True
            
            else: 
                self.timer += 1

    def dealDealer(self):
        if self.timer <= 60 and self.timer % 15 == 0:
            if not self.dealer.getBust() and not self.dealer.doneDeal() and not self.dealer.getValue() >= self.dealerThreshold:
                self.dealer.cards.append(self.deck.cards[0])
                self.deck.cards.pop(0)
            self.timer += 1

        elif self.timer > 60 or self.dealer.getValue() >= self.dealerThreshold:
            self.timer = 0
            self.done = 0
            self.dealing = False
            self.dealerDealing = False
            self.checkingWin = True
        
        else: 
            self.timer += 1

    def hit(self):
        # print(self.activeHand)
        if self.activeHand == 1:
            if not self.player.hand.isStood:
                if len(self.player.hand.cards) < 5:
                    self.dealCard(self.player.hand)

                if self.player.hand.getBust():
                    self.player.hand.isStood = True
                    if len(self.player.hand2.cards) == 0 or self.player.hand2.isStood:
                        self.dealAI()
                    else:
                        self.player.activeHand = 2

                elif self.player.hand.getValue() == 21:
                    self.player.hand.isStood = True
                    if len(self.player.hand2.cards) == 0 or self.player.hand2.isStood:
                        self.dealAI()
                    else:
                        self.player.activeHand = 2

                elif len(self.player.hand.cards) == 5:
                    self.player.hand.isStood = True
                    if len(self.player.hand2.cards) == 0 or self.player.hand2.isStood:
                        self.dealAI()
                    else:
                        self.player.activeHand = 2
        else:
            if not self.player.hand2.isStood:
                if len(self.player.hand2.cards) < 5:
                    self.dealCard(self.player.hand2)
                
                if self.player.hand2.getBust():
                    self.player.hand2.isStood = True
                    self.player.activeHand = 1
                    if self.player.hand.isStood:
                        self.dealAI()

                elif self.player.hand2.getValue() == 21:
                    self.player.hand2.isStood = True
                    if self.player.hand.isStood:
                        self.dealAI()
                    else:
                        self.player.activeHand = 1

                elif len(self.player.hand2.cards) == 5:
                    self.player.hand2.isStood = True
                    if self.player.hand.isStood:
                        self.dealAI()
                    else:
                        self.player.activeHand = 1
            
    def stand(self):
        if len(self.player.hand2.cards) == 0:
            self.player.hand.isStood = True
            self.dealAI()

        else:
            if self.activeHand == 1:
                self.player.hand.isStood = True
                if not self.player.hand2.isStood:
                    self.activeHand = 2
                else:
                    self.dealAI()

            elif self.activeHand == 2:
                self.player.hand2.isStood = True
                if not self.player.hand.isStood:
                    self.activeHand = 1
                else:
                    self.dealAI()

    def split(self):
        if self.money > 0:
            self.activeHand = 2
            self.bet = 0
            self.betted = False
            self.player.hand2.cards.append(self.player.hand.cards[1])
            self.player.hand.cards.pop(1)

    def placeBet(self):
        # Entering Bet
        if self.bet > 0 and self.bet <= self.money:
            self.money -= self.bet
            self.betted = True

            if self.activeHand == 1:
                self.player.hand.bet = self.bet
            elif self.activeHand == 2:
                self.player.hand2.bet = self.bet
                self.activeHand = 1

    def checkWin(self):
        font = pygame.font.SysFont(self.fontFam, 20)
        handText = "Bet on Hand 1: $" + str(self.player.hand.bet)
        hand2Text = "Bet on Hand 2: $" + str(self.player.hand2.bet)

        if self.timer <= 45:
            # Check hand 1
            if not self.player.hand.getValue() == self.dealer.getValue():
                if self.player.hand.getValue() == 21:
                    self.won = True
                elif self.player.hand.getValue() > self.dealer.getValue() and not self.player.hand.getBust():
                    self.won = True
                elif self.dealer.getBust() and not self.player.hand.getBust():
                    self.won = True
                elif len(self.player.hand.cards) == 5 and not self.player.hand.getBust():
                    self.won = True

            elif self.player.hand.getValue() == self.dealer.getValue() and not self.player.hand.getBust():
                self.drew = True

            # Check Hand 2
            if not self.player.hand2.getValue() == self.dealer.getValue():
                if self.player.hand2.getValue() == 21:
                    self.won2 = True
                elif self.player.hand2.getValue() > self.dealer.getValue() and not self.player.hand2.getBust():
                    self.won2 = True
                elif self.dealer.getBust() and not self.player.hand2.getBust():
                    self.won2 = True
                elif len(self.player.hand2.cards) == 5 and not self.player.hand2.getBust():
                    self.won2 = True

            elif self.player.hand2.getValue() == self.dealer.getValue() and not self.player.hand2.getBust():
                self.drew2 = True

            if self.won:
                screen.blit(font.render("Won", True, self.red), (20 + font.size(handText)[0], 55))
            elif self.drew:
                screen.blit(font.render("Drew", True, self.red), (20 + font.size(handText)[0], 55))
            else:
                screen.blit(font.render("Lost", True, self.red), (20 + font.size(handText)[0], 55))

            if len(self.player.hand2.cards) > 0:
                if self.won2:
                    screen.blit(font.render("Won", True, self.red), (20 + font.size(handText)[0], 85))
                elif self.drew2:
                    screen.blit(font.render("Drew", True, self.red), (20 + font.size(handText)[0], 85))
                else:
                    screen.blit(font.render("Lost", True, self.red), (20 + font.size(handText)[0], 85))

            self.timer += 1
        
        else:
            if self.won:
                self.won = False
                self.money += self.player.hand.bet * 2
            elif self.drew:
                self.drew = False
                self.money += self.player.hand.bet
            
            if self.won2:
                self.won2 = False
                self.money += self.player.hand2.bet * 2
            elif self.drew2:
                self.drew2 = False
                self.money += self.player.hand2.bet

            if self.money == 0:
                self.gameOver = True
            else:
                self.checkingWin = False
                self.timer = 0
                self.resetGame()

    # Game setup functions
    def gameSetup(self):
        self.player = Player()

        for i in self.aiHands:
            i.cards.clear()
            i.isStood = False
            i.isBust = False

        self.dealer = Hand()

        self.deck = Deck(self.stacks)
        self.deck.shuffle(self.shuffleCount)

        self.game = True

    def firstDeal(self):
        # print(self.timer % 15)
        # print("timer: " + str(self.timer))
        self.activeHand = 1

        if self.timer <= 15 and self.timer % 15 == 0:
            self.dealCard(self.player.hand)

            # for c in self.player.hand.cards:
            #     print("card: " + c.face + str(c.suit))

            self.dealer.cards.append(self.deck.cards[0])
            self.deck.cards.pop(0)

            for i in self.aiHands:
                self.dealCard(i)

            self.timer += 1
        
        elif self.timer > 15:
            self.initialDeal = False
            self.timer = 0
        
        else:
            self.timer += 1

        # If player gets a natural blackjack
        if self.player.hand.getValue() == 21:
            self.player.hand.isStood = True
            self.revealTime = True
            self.initialDeal = False
            self.dealerDealing = True
            self.timer = 0

    def resetGame(self):
        del self.player
        self.timer = 0
        self.done = 0
        self.initialDeal = True
        self.revealTime = False
        self.dealing = False
        self.dealerDealing = False
        self.bet = 0
        self.activeHand = 1
        self.betted = False
        self.gameSetup()
        
    # Drawing UI
    def drawGame(self):
        count = len(self.dealer.cards)
        xRange  = self.aiCount + 1
        font = pygame.font.SysFont(self.fontFam, 20)
        font2 = pygame.font.SysFont(self.fontFam, 42)
        font3 = pygame.font.SysFont("Verdana", 42)

        self.drawPlayer()

        # Draw money labels
        currentBet = "Hand 1 Bet: $" + str(self.player.hand.bet)
        currentBet2 = "Hand 2 Bet: $" + str(self.player.hand2.bet)
        moneys = "Money: $" + str(self.money)

        # Your money
        screen.blit(font.render(moneys, True, self.black), (25, 25))
        # Hand 1 bet
        screen.blit(font.render(currentBet, True, self.black), (25, 55))

        if self.player.hand2.bet > 0:
            screen.blit(font.render(currentBet2, True, self.black), (25, 85))
            if not self.checkingWin:
                if self.activeHand == 1:
                    screen.blit(font.render("Current Hand", True, self.red), (30 + font.size(currentBet)[0], 55))
                else:
                    screen.blit(font.render("Current Hand", True, self.red), (30 + font.size(currentBet2)[0], 85))

        # Draw interaction buttons
        if not self.initialDeal and not self.dealing and not self.checkingWin and not self.dealerDealing:
            # hands = [self.player.hand, self.player.hand2]
            # if not hands[self.activeHand - 1].getBust():
                self.hitButton = screen.blit(font.render("Hit", True, self.black), (970 - (font.size("Hit")[0]), 25))
                self.standButton = screen.blit(font.render("Stand", True, self.black), (970 - (font.size("Stand")[0]), 55))

                if len(self.player.hand2.cards) == 0 and len(self.player.hand.cards) <= 2:
                    if self.player.hand.cards[0].face == self.player.hand.cards[1].face:
                        self.splitButton = screen.blit(font.render("Split", True, self.black), (970 - (font.size("Split")[0]), 85))
                elif len(self.player.hand2.cards) >= 1:
                    self.switchButton = screen.blit(font.render("Switch Hand", True, self.black), (970 - (font.size("Switch Hand")[0]), 85))

        # Draw each card in dealer hand
        for c in range(count):
            cc = 0.5*count*100
            x = 500 - cc + c*100
            text = str(self.dealer.cards[c].suit) + str(self.dealer.cards[c].face)
            pygame.draw.rect(screen, self.white, (x, 0, 100, 125))

            if self.betted:
                # Draw Dealer
                if not self.revealTime:
                    # Only draw first card before reveal
                    if c == 0:
                        screen.blit(font2.render(text, True, self.black), ((x + 75) - font2.size(text)[0], 82 - font2.size(text)[1]))
                    else:
                        screen.blit(font3.render("?", True, self.black), ((x + 75) - font3.size("?")[0], 82 - font3.size("?")[1]))
                else:
                    screen.blit(font2.render(text, True, self.black), ((x + 75) - font2.size(text)[0], 82 - font2.size(text)[1]))
        
        if self.aiCount == 1 or self.aiCount == 3:
            xx = (1000 - font.size("Total: " + str(self.dealer.getValue()))[0])/2
            yy = 130

        else:
            cc = 0.5*count*100
            x = 500 - cc + count*100
            # xx = (900 - font.size("Total: " + str(self.dealer.getValue()))[0])/2
            xx = x + 25
            yy = 50

        # Draw dealer value
        if not self.dealer.getBust():

            if self.revealTime:
                screen.blit(font.render("Total: " + str(self.dealer.getValue()), True, self.black), (xx, yy))
            else:
                screen.blit(font.render("Total: ?", True, self.black), (xx, yy))
        
        elif self.dealer.getValue() == 21:
            if self.aiCount == 1 or self.aiCount == 3:
                xx = (1000 - font.size("Blackjack")[0])/2
            screen.blit(font.render("Blackjack", True, self.black), (xx, yy))

        else:
            if self.aiCount == 1 or self.aiCount == 3:
                xx = (1000 - font.size("Bust")[0])/2
            screen.blit(font.render("Bust", True, self.black), (xx, yy))

        # Draw AI
        for a in range(self.aiCount):
            x = (1000/xRange)*(a + 1.5) - 50
            val = "Total: " + str(self.aiHands[a].getValue())

            # Draw each card in hand
            for i in self.aiHands[a].cards:
                txt = str(i.suit) + str(i.face)
                pygame.draw.rect(screen, self.red, (x, 675 - (130*self.aiHands[a].cards.index(i)), 100, 125))
                if self.betted:
                    screen.blit(font2.render(txt, True, self.black), ((x + 82) - font2.size(txt)[0], 700 - (130*self.aiHands[a].cards.index(i))))
                yy = 700 - (130*self.aiHands[a].cards.index(i))
            
            if not self.aiHands[a].getBust():
                # Draw hand value
                screen.blit(font.render(val, True, self.black), (x + 80 - font.size(val)[0], yy - 50))
            elif self.aiHands[a].getValue() == 21:
                screen.blit(font.render("Blackjack", True, self.black), (x + 80 - font.size("Blackjack")[0], yy - 50))
            else:
                #If Bust:
                screen.blit(font.render("Bust", True, self.black), (x + 80 - font.size("Bust")[0], yy - 50))
                  
    def drawPlayer(self):
        xRange  = self.aiCount + 1
        x = (1000/xRange/2) - 50
        font = pygame.font.SysFont(self.fontFam, 20)
        font2 = pygame.font.SysFont(self.fontFam, 42)
        yy = 700

        if self.activeHand == 1:
            # Draw the "hand"
            for i in self.player.hand.cards:
                text = str(i.suit) + str(i.face)
                pygame.draw.rect(screen, self.yellow, (x, 675 - (130*self.player.hand.cards.index(i)), 100, 125))
                if self.betted:
                    screen.blit(font2.render(text, True, self.black), (x + 82 - font2.size(text)[0], 700 - (130*self.player.hand.cards.index(i))))
                yy = 700 - (130*self.player.hand.cards.index(i))

            if not self.player.hand.getBust():
                if not self.player.hand.getValue() == 21:
                    # Draw hand value
                    txt = "Total: " + str(self.player.hand.getValue())
                    screen.blit(font.render(txt, True, self.black), (x + 80 - font.size(txt)[0], yy - 50))
                else:
                    screen.blit(font.render("Blackjack", True, self.black), (x + 80 - font.size("Blackjack")[0], yy - 50))
            else:
                #If Bust:
                screen.blit(font.render("Bust", True, self.black), (x + 75 - font.size("Bust")[0], yy - 50))
        
        else:
            # Draw second hand
            for i in self.player.hand2.cards:
                text = str(i.suit) + str(i.face)
                pygame.draw.rect(screen, self.yellow, (x, 675 - (130*self.player.hand2.cards.index(i)), 100, 125))
                if self.betted:
                    screen.blit(font2.render(text, True, self.black), (x + 82 - font2.size(text)[0], 700 - (130*self.player.hand2.cards.index(i))))
                yy = 700 - (130*self.player.hand2.cards.index(i))

            if not self.player.hand2.getBust():
                if not self.player.hand2.getValue() == 21:
                    # Draw hand value
                    txt = "Total: " + str(self.player.hand2.getValue())
                    screen.blit(font.render(txt, True, self.black), (x + 80 - font.size(txt)[0], yy - 50))
                else:
                    screen.blit(font.render("Blackjack", True, self.black), (x + 80 - font.size("Blackjack")[0], yy - 50))
            
            elif self.player.hand2.getValue() == 21:
                screen.blit(font.render("Blackjack", True, self.black), (x + 80 - font.size("Blackjack")[0], yy - 50))

            else:
                #If Bust:
                screen.blit(font.render("Bust", True, self.black), (x + 75 - font.size("Bust")[0], yy - 50))

    def drawBetting(self):
        moneyText = "Your Money: $" + str(self.money)
        bet = "$" + str(self.bet)
        betText = "Enter Bet"

        screen.blit(self.font3.render(moneyText, True, self.black), (500 - (self.font3.size(moneyText)[0]/2), 150))
        self.betButton = screen.blit(self.font1.render("Bet", True, self.black), (500 - (self.font1.size("Bet")[0]/2), 500))

        if self.bet == 0:
            screen.blit(self.font2.render(betText, True, self.red), (500 - (self.font2.size(betText)[0]/2), 300))
        else:
            screen.blit(self.font2.render(bet, True, self.red), (500 - (self.font2.size(bet)[0]/2), 300))

if __name__ == "__main__":
    # Initialising
    pygame.init()
    screen = pygame.display.set_mode((1000,800))
    pygame.display.set_caption("Jack")
    Main()