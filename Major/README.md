# HSC Major Work 2021

## Prerequisites

### Hardware Requirements

#### Minimum Specs

- 64-bit OS:
  - Windows 7+
  - Mac OSX 10.11+ (not tested)
  - Linux (not tested)
- x86 64-bit CPU (Intel/AMD)
- 4 GB RAM
- 5 GB Disk space
- Direct X 9.0 compatible GPU with OpenGL Support

Note that support for Origin, Uplay and Epic Games are only present on Windows.

#### Recommended Specs

- Windows 10 64-Bit
- 8+ GB RAM
- 5+ GB Disk Space
- Intel HD 630/AMD Radeon R5 240/Nvidia GT630 or higher

### Software Prerequisites

- Python 3.6+
- Required Python modules are listed in requirements.txt and can be installed by running `pip install -r requirements.txt` in the same directory as `requirements.txt`.
  - Note `steamfiles` requires `pip` version `9.0.3` or lower to install properly. To downgrade your pip, run `pip install --user pip==9.0.3` in your terminal before running `pip install -r requirements.txt`. To upgrade pip afterwards, run `python -m pip install --user --upgrade pip`.

### Other Requirements

To build this program from the source code, you will need to provide your own secrets.json file in the `src/python/ext_api` folder. The file should be formatted as such:

```json
{
    "igdb": <igdb secret>,
    "igdb_id": <igdb client id>,
    "itad": <itad secret>,
    "itad_id": <itad client id>,
    "itad_api": <itad api key>
}
```

You may obtain your IGDB and ITAD keys by following the instructions [here](https://api-docs.igdb.com/#about) and [here](https://itad.docs.apiary.io/#introduction/do's-and-don'ts )

User manual is provided in `/doc`

## Problem Definition

The recent increase in different game launchers and stores, such as the release of the Epic Game Store in 2018, have left gamers finding that their games no longer exist on only 1 store. For a large period of time, most PC games have been distributed on Valve's online platform: Steam. Whilst "third-party" game stores such as UPlay and Origin have existed, for the most part these stores have integrated with Steam to an acceptable extent. Within the past 5 years, there has been a great increase in interest for DRM free games from sites such as GOG. In addition, the arrival of a new market in the form of the Epic Games Store, as well as (to a lesser extent) the presence of games on the Windows Store, have led to the departure of game developers from established platforms such as Steam. This has left gamers finding that their games are in more fragmented locations now. The presence of so many game launchers on one system is a headache for many gamers, especially if they frequently switch through games on different stores/launchers. In addition to this, gamers who are looking to purchase games, and find deals on games may find it extremely difficult navigating through the store pages on multiple launchers/platforms. Gamers utilising wishlists and notifications may also find it difficult to sync these wishlists, and the scattering of notifications from different platforms (and potentially at different times) may add to the cluttered nature of PC gaming today. While solutions to both issues currently exist in the form of third party game managers, and deal finding software/websites; a single solution to solve both problems does not seem to be on the market. This project will aim to create a piece of software which allows users to manage their games from different stores (with stats and game information where possible), as well as providing users with a single software for syncing wishlists and notifications for game deals.

## Needs and Objectives

### Needs

- Due to the interaction of APIs and the need for separate modules in this software, object-oriented design must be used in order to ensure efficient development and maintainance of this software.
- Due to the relatively little time for development of this major work, adequate planning is critical to the success of this project.
- This software must be able to present user's game library/libraries in a simple, intuitive UI.
- The software must be able properly to interface and communicate with APIs in order to deliver basic functions required in the software solution (i.e. syncing user wishlists, importing/exporting wishlists, integrated deal searching), as well as for implementing other features defined as objectives (i.e. Steam achievements integration).
- Basic UI scaling should be implemented (i.e. window resizing) as it is a basic QoL thing.
- The software MUST responsibly, and securely access APIs which may read/write user data. This is a social ethical issue which is critical as this software must not endanger the privacy/security of the user and their data.
- This program must allow users to add games to their library, and to be able to launch them and adjust basic settings such as launch options, or giving the game a custom nickname.
- This program must be able to properly launch games which require 3rd party launchers (i.e. Steam/uPlay), and interact with these 3rd party launchers in a lightweight fashion without interfering with the graceful operation of such games and launchers.

### Objectives

- As this project is intended to be a replacement game library/manager and to be an all in one solution for the user, it would be ideal if the project is bug-free (or as close to as possible) in order to provide a positive user experience, and maintain a userbase.
- Ideally, this project should integrate achievement tracking, and such features present in current game launchers such as Steam or Playnite.
- If time permits, cross-platform compatibility would be an important objective as it will allow users to use 1 solution across multiple operating systems.
- The ability to scan through the user's file system to find existing games and/or game launcher installations would be ideal, and would greatly automate the users' workflow.
- Ideally some interface customisability (ie. language, colours) would be ideal, as that would be more inclusive for people of different needs/locales.
- With the increase in high resolution and Hi-DPI displays, proper Hi-DPI scaling would be ideal.
- Different ways to sort the games listed. This would include things like sorting by alphabet, last played, most hours, etc. This allows for a more customised user experience.
- Integration with emulators. This would be a nice QOL feature, where this software can launch emulated games and such directly, without having the user manually launch the game from the emulator menu.

## Boundaries and Constraints

### Boundaries

- Technological
  - GUI - GUI design is complex, and as the purpose of this software is to be lightweight and simple, the GUI should follow the same philosophy. The layout should be easy to navigate, and not overly complex.
  - Level of integration with external services - This software should be lightweight and siimple, and should only contain the necessary features required to meet its objective. As such, certain features of the external services required (i.e. IsThereAnyDeal) may need to be excluded for simplicity's sake.
- Social and Ethical
  - Inclusivity and ergonomics - This program should be usable by people from all walks of lives, and of all abilities. The interface should be simple and understandable, and colour combinations and graphical choices should accommodate for any vision conditions.

### Constraints

- Technological
  - Technological resources - As this program is for the general populace of users, this game needs to be functional on a wide range of differently spec'd hardware (and possibly different operating systems) and as such, needs to be lightweight and as efficient as possible.
    - In addition, the hardware I have on hand will affect how well I am able to develop certain features for certain platforms (i.e. I may not be able to focus on maintaining compatibility of certain features with Linux as I do not have a Linux system on hand at this time.)
  - Time constraints - As this project has a deadline, the amount of features, and the complexity of features will need to be considered in order for the program to be delivered within these boundaries.
  - Bugs - Due to time and coding efficiency, bugs are inevitable. Therefore, we must attempt to squash as many of them as possible (all of them ideally), but they still impose an annoying boundary if left unchecked.
  - Technical ability - As I am a programming student, the extent of my programming capabilities and knowledge will have an effect on what and how features are implemented.
  - Cross-platform compatibility - Whilst ideally this program should work with most operating systems, some features will be missing due to a.) time constraints meaning I may not be able to implement certain features across platforms, and b.) certain methods required for certain features (i.e. winreg for uPlay identification).
- Social and Ethical
  - Intellectual property - As this program interfaces with many APIs and will most likely incorporate modules and/or utilise services provided with limited licenses; it is crucial that this software must follow the terms associated with these licenses, so as to not violate copyright law. In addition, any interaction/usage of media from games imported/accessed by this software must be used responsibly.

## Systems Modelling

Diagrams will be provided in the /doc folder.

### Data Dictionary

**Field Name**|**Data Type**|**Data Format**|**Bytes for Storage**|**Size for Display**|**Description**|**Example**|**Validation**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
Game|object|{name: string, game-id: string, game-path: string, hours: int}| | |Object to store each individual game entry. The game-id is to be formatted as string as whilst Steam utilises a numerical numbering system, other platforms such as Epic utilise codenames i.e. Snapdragon.|{name: "Counter-Strike: Global Offensive", game-id: "730", game-path: "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Counter-Strike Global Offensive"}|
self.igdb.client\_id|string| |2/char| |A unique ID used to identify the client to the API.|gduieoaj467387424ytgjkdhf98gy24gtyg72gf|
self.igdb.client\_secret|string| |2/char| |A secret used by the application and authorisation server to protect resources and grant tokens.|vox8876wag6s7satug324j325bhj4yu2356g|
self.dir|string| |2/char| |The directory in which the application is running.|C:\\Windows\\System32|Valid directory in the filesystem
self.games|array|[Game, Game, ...] | | |An array to store game entry objects.|[<\_\_main\_\_.Game object at 0x000002107F8D1FD0>, <\_\_main\_\_.Game object at 0x000002107F8D1FD2>, ...]|
response.status\_code|int|NNN|3| |The response code from the POST request. Used to identify if the request was successful.|200|A value between 100 - 599
self.wh|tuple|(NNN…, NNN…)| | |A tuple to store the width and height of the application window.|(1920, 1080)|
self.playIcon|object|Refer to QPixmap in PySide6 Documentation||4096|A QPixmap object to display an image|QPixmap("/users/e/icons/play.png", "png")|A valid QPixmap object.
self.gameIDs|array||||An array to store game identifiers|["730", "Snapdragon", "42069", ...]
self.platform|string||2/char||A string to identify the operating system|"darwin"

## Feasibility Study

For this project to be feasible, we must ensure that the project meets the necessary social and ethical requirements, and legal requirements in order to succeed. In addition, consideration needs to be taken in terms of other products available in the current market, and whether or not this project will be able to compete with existing solutions (if any).

### Social and Ethical feasibility

To create a socially and ethically feasible product, the following issues must be addressed.

#### Inclusivity

With the ease of access of technology in modern times, software is accessible to people of all levels of ability. It would be a social and ethical consideration to ensure that this software caters to most, if not all people of different abilities. Ergonomics of the user interface must be considered in order to ensure that the program is easy to use for people of all abilities. The program should follow ergonomics conventions such as consistency, simplicity, proper user feedback, display conventions (i.e. high contrast colours and properly sized elements), and political correctness, as well as offering some method of accomodating user differences (i.e. user preferences, allowing adjustment of elements such as screen layout/size, appearance, icons, etc.).

#### Localisation

With the global scale of software in modern times, it is highly likely that users of this program will be from many different localities. As such, it would be a social and ethical consideration to ensure that the features and functions of the program can be understood and utilised by people of different localities. The usage of universal symbols and/or colours are a simple way to ensure that the majority of users are able to understand their meaning. In addition, local translations will also improve the usability of this program across different localities.

#### Data Quality and Security

As this program may be accessing and modifying sensitive user information such as account details, game and purchase histories, wish-lists, and other potentially personal information, the access of such information should be performed in a secure, and error free way in order to protect the privacy and integrity of the user's data. In additional, any calls for retrieving and/or modifying data must be error free, and properly formatted to ensure that only the relevant and necessary information is accessed, and in a secure manner.

### Legal feasibility

To avoid serious legal repercussions, the following concerns must be properly addressed for this project to be feasible.

#### Intellectual Property

As this project will be using external code, and referencing and interacting with external media, proper treatment of intellectual property, and copyright laws must be adhered to. In the usage of external APIs, some of which are proprietary, we must ensure that any code and/or data obtained/used from these external services must be properly referenced and acknowledged. Any code snippets which this project uses should be original, and should external code or libraries be used, they must be used in accordance to any copyrights and licenses which should apply.

In addition, in interacting with 3rd party media such as game artwork and other 3rd party trademarks, care should be taken in the proper usage of such media. Any external media should be used in accordance to any license or copyright which may apply to such media, and it is up to the developer to properly distinguish between any original media, and 3rd party, licensed, or trademarked media.

### Other products available

Currently, 3rd party game launchers/game library managers do exist. Solutions such as [Playnite](https://lutris.net/), [Lutris](https://lutris.net/), [GameHub](https://tkashkin.tk/projects/gamehub/), [LaunchBox](https://www.launchbox-app.com/), and [GOG Galaxy](https://www.gogalaxy.com/en/) to name a few. However, such solutions often include too many features, which is good for users who take advantage of those features, but clutters the UI and makes the software more complicated. In addition, not many of them have integration with deals software/APIs and/or game stores such as [IsThereAnyDeal](https://isthereanydeal.com). Furthermore, some of these projects are a little dated in their UI, and user friendliness. My software aims to address only the 2 simple tasks that a game manager should include: having games launch-able from one central location, and to have integration with a games deal finder. This distinguishes my software from the rest of the market, as I am providing a simple, and lightweight utility, which will appeal to people who are looking for only the essentials.

## Research Reports

Reports will be provided in the /doc folder.

## Sales Presentation

In /doc folder.
