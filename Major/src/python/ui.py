from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *

import os, platform, json, webbrowser

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        
        # Get directory where we are running from
        self.dir = os.path.dirname(__file__)

        # Misc. stylesheet stuff
        MainWindow.setStyleSheet(
            "QToolBar {padding-left:8; padding-right:8; padding-top: 0; padding-bottom: 0};"
        )

        # Set window size
        MainWindow.setMinimumSize(800, 700)
        MainWindow.resize(QSize(800, 700))

        # Set up toolbar
        self.drawToolbar(MainWindow)

        # Set up the individual pages of the cetnral widget stack

        # Game Library Page
        self.page1_layout = QGridLayout()

        try:
            self.drawTable()

        except:
            self.page1_scanButton = QPushButton("Scan for Games")
            self.page1_scanButton.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
            self.page1_scanButton.setFont(QFont("Arial", 36))
            self.page1_layout.addWidget(self.page1_scanButton)

        self.page1 = QWidget()
        self.page1.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.page1.setLayout(self.page1_layout)
        
        # Manage Game Page
        self.page2_layout = QGridLayout()

        self.page2 = QWidget()
        self.page2.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.page2.setLayout(self.page2_layout)

        # Game Deals Page
        self.page3_layout = QFormLayout()
        searchLabel = QLabel("Find Games")
        searchLabel.setFont(QFont("Arial", 24))
        searchLabel.setAlignment(Qt.AlignCenter)

        self.searchBar = QLineEdit()
        self.searchBar.setAlignment(Qt.AlignCenter)
        self.searchBar.setFont(QFont("Arial", 18))
        self.searchBar.setPlaceholderText("Enter game to find deals for")
        self.searchBar.setToolTip("Enter game to search for")
        self.searchButton = QPushButton("Search IsThereAnyDeal.com")
        self.searchButton.setMinimumSize(400, 50)
        self.searchButton.setFont(QFont("Arial", 16))
        self.resultsTable = QTableWidget()
        self.resultsTable.setSortingEnabled(True)
        self.resultsTable.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.resultsTable.setStyleSheet("QTableWidgetItem {font-size: 24; text-align: center;}")

        self.page3_layout.addRow(searchLabel)
        self.page3_layout.addRow(self.searchBar)
        self.page3_layout.addRow(self.searchButton)
        self.page3_layout.addWidget(self.resultsTable)

        self.page3 = QWidget()
        self.page3.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.page3.setLayout(self.page3_layout)

        # Set up central widget stack
        self.stack = QStackedWidget(MainWindow)
        self.stack.setObjectName(u"stack")
        self.stack.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.stack.addWidget(self.page1)
        self.stack.addWidget(self.page2)
        self.stack.addWidget(self.page3)

        # Set up menubar
        # self.menubar = QMenuBar(MainWindow)
        # self.menubar.setObjectName(u"menubar")

        # Set up menu actions
        # self.menuAdd = QAction("Add game...", MainWindow)
        # self.menuAdd.setObjectName("menuAdd")
        # self.menuScan = QAction("Scan for games...", MainWindow)
        # self.menuScan.setObjectName("menuScan")
        # self.menuPreferences = QAction("Preferences", MainWindow)
        # self.menuPreferences.setObjectName("menuPreferences")

        # Setup menubar categories
        # self.menuFile = QMenu(self.menubar)
        # self.menuFile.setObjectName(u"menuFile")
        # self.menuFile.addAction(self.menuAdd)
        # self.menuFile.addAction(self.menuScan)
        # self.menuFile.addAction(self.menuPreferences)

        # self.menuEdit = QMenu(self.menubar)
        # self.menuEdit.setObjectName(u"menuEdit")

        # self.menuView = QMenu(self.menubar)
        # self.menuView.setObjectName(u"menuView")

        # self.menubar.addMenu(self.menuFile)
        # self.menubar.addMenu(self.menuEdit)
        # self.menubar.addMenu(self.menuView)

        # Append actions to menubar
        # self.menubar.addAction(self.menuFile.menuAction())
        # self.menubar.addAction(self.menuEdit.menuAction())
        # self.menubar.addAction(self.menuView.menuAction())

        # Set up MainWindow Widgets and Menubar
        MainWindow.setCentralWidget(self.stack)
        # MainWindow.setMenuBar(self.menubar)

        # self.retranslateUi(MainWindow)

        # Set default page on startup
        self.stack.setCurrentIndex(0)

        QMetaObject.connectSlotsByName(MainWindow) # connect slots by name or something


    # Translate UI. Put here by Qt Creator dunno if I'll have time to actually do this.
    # def retranslateUi(self, MainWindow):
    #     MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"main", None))
    #     self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        # self.menuEdit.setTitle(QCoreApplication.translate("MainWindow", u"Edit", None))
        # self.menuView.setTitle(QCoreApplication.translate("MainWindow", u"View", None))

    def drawToolbar(self, MainWindow):
        # Define a spacer
        self.spacer = QWidget()
        self.spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        # Defining all toolbar options
        self.addAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/add.png", "png")), "Add Games", MainWindow)
        self.scanAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/scan.png", "png")), "Scan for Games to Add", MainWindow)
        self.playAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/play.png", "png")), "Launch Game", MainWindow)
        self.manageAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/manage.png", "png")), "Manage Game", MainWindow)
        self.itadAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/find.png", "png")), "Find game deals with IsThereAnyDeal.com", MainWindow)
        self.settingsAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/settings.png", "png")), "Settings", MainWindow)
        self.libraryAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/back.png", "png")), "Back to My Game Library", MainWindow)
        self.backAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/back.png", "png")), "Back", MainWindow)
        self.connectITADAction = QAction(QIcon(QPixmap(f"{self.dir}/icons/connect.png", "png")), "Authenticate with IsThereAnyDeal.com", MainWindow)

        # Connect Actions
        self.itadAction.triggered.connect(lambda:self.switchTab(2))
        self.backAction.triggered.connect(lambda:self.switchTab(0))
        self.libraryAction.triggered.connect(lambda:self.switchTab(0))
      
        # Assign actions and elements to different toolbars
        self.tb1Actions = [self.playAction, self.manageAction, self.addAction, self.scanAction, self.spacer, self.settingsAction, self.itadAction]
        self.tb2Actions = [self.backAction, self.spacer, self.settingsAction, self.itadAction]
        self.tb3Actions = [self.libraryAction, self.spacer, self.settingsAction, self.connectITADAction]

        # List of toolbars
        self.toolbars = [self.tb1Actions, self.tb2Actions, self.tb3Actions]

        # Set up toolbar for initial page
        self.toolbar = MainWindow.addToolBar("Toolbar")

        for action in self.toolbars[0]:
            if action != self.spacer:
                self.toolbar.addAction(action)
            else:
                self.toolbar.addWidget(action)

        self.toolbar.setObjectName(u"toolbar")
        self.toolbar.setMovable(False)
        self.toolbar.setIconSize(QSize(84,42))

    def drawTable(self):
        # Load game paths file
        with open(f"{self.dir}/games.json") as g:
            games = json.load(g)
            h = len(games)
            n = 0

            # Set up the table widget and properties
            self.tableWidget = QTableWidget()
            self.tableWidget.setSortingEnabled(True)
            self.tableWidget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
            self.tableWidget.setToolTip("Double-click to launch game. Single click to select.")

            # Set the stylesheet
            self.tableWidget.setStyleSheet("QTableWidgetItem {font-size: 24; text-align: center;}")

            # Table Behaviour
            self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
            self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
            self.tableWidget.verticalHeader().setVisible(False)

            # Table Size
            self.tableWidget.setColumnCount(4)
            self.tableWidget.setRowCount(h)

            # Populate the table with the games
            for game in games:

                # Set the right platform icon
                if games[game]["launcher"] == "steam":
                    self.tableWidget.setCellWidget(n, 0, Icon("steam"))
                elif games[game]["launcher"] == "uplay":
                    self.tableWidget.setCellWidget(n, 0, Icon("uplay"))
                elif games[game]["launcher"] == "origin":
                    self.tableWidget.setCellWidget(n, 0, Icon("origin"))
                elif games[game]["launcher"] == "epic":
                    self.tableWidget.setCellWidget(n, 0, Icon("epic"))
                else:
                    self.tableWidget.setCellWidget(n, 0, Icon("standalone"))

                # formatting the widget stuff
                gameName = QTableWidgetItem(game)
                gameName.setTextAlignment(Qt.AlignCenter)
                gamePath = QTableWidgetItem(games[game]["path"])
                gamePath.setTextAlignment(Qt.AlignCenter)
                gameID = QTableWidgetItem(games[game]["id"])
                gameID.setTextAlignment(Qt.AlignCenter)

                # set the game properties and put them in the talble
                self.tableWidget.setItem(n, 1, gameName)
                self.tableWidget.setItem(n, 2, gamePath)
                self.tableWidget.setItem(n, 3, gameID)

                n += 1

            # header cosmetic tings
            header = self.tableWidget.horizontalHeader()
            header.setSectionResizeMode(2, QHeaderView.Stretch)
            header.setDefaultAlignment(Qt.AlignCenter)
            header.setFixedHeight(48)
            self.tableWidget.setHorizontalHeaderLabels(["Platform","Title","Location","Internal ID"])

            # set font
            font = self.tableWidget.font()
            font.setPointSize(12)
            self.tableWidget.setFont(font)

            # set the widths for the columns
            self.tableWidget.setColumnWidth(0,85)
            self.tableWidget.setColumnWidth(1,250)
            self.tableWidget.setColumnWidth(3,100)

            # set row height for consistency
            for row in range(self.tableWidget.rowCount()):
                self.tableWidget.setRowHeight(row, 85)

            # add the table to the page.
            self.page1_layout.addWidget(self.tableWidget)

    def drawResultsTable(self, items):
        height = len(items)
        n = 0

        # this stuff is basically the same as before.

        self.resultsTable = QTableWidget()
        self.resultsTable.setSortingEnabled(True)
        self.resultsTable.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.resultsTable.setStyleSheet("QTableWidgetItem {font-size: 24; text-align: center;}")
        self.resultsTable.setToolTip("Double-click to view on IsThereAnyDeal.com")

        # Table Behaviour
        self.resultsTable.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.resultsTable.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.resultsTable.verticalHeader().setVisible(False)

        # Table Size
        self.resultsTable.setColumnCount(4)
        self.resultsTable.setRowCount(height)

        # Populate the table with the games
        for item in items:
            title = items[item]["title"]

            if items[item]["lowest"] is not None:
                lowest = str(items[item]["lowest"])
            else:
                lowest = "-"
            
            if items[item]["current"] is not None:
                current = str(items[item]["current"])
            else:
                current = "-"

            if items[item]["currentStore"] is not None:
                currentStore = items[item]["currentStore"]
            else:
                currentStore = "-"

            itemTitle = QTableWidgetItem(title)
            itemTitle.setTextAlignment(Qt.AlignCenter)
            itemLowest = QTableWidgetItem(lowest)
            itemLowest.setTextAlignment(Qt.AlignCenter)
            itemCurrent = QTableWidgetItem(current)
            itemCurrent.setTextAlignment(Qt.AlignCenter)
            itemCurrentStore = QTableWidgetItem(currentStore)
            itemCurrentStore.setTextAlignment(Qt.AlignCenter)

            self.resultsTable.setItem(n, 0, itemTitle)
            self.resultsTable.setItem(n, 1, itemLowest)
            self.resultsTable.setItem(n, 2, itemCurrent)
            self.resultsTable.setItem(n, 3, itemCurrentStore)

            n += 1

        header = self.resultsTable.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setDefaultAlignment(Qt.AlignCenter)
        header.setFixedHeight(48)
        self.resultsTable.setHorizontalHeaderLabels(["Title","Lowest Ever Price","Current Lowest Price","Current Lowest Store"])

        font = self.resultsTable.font()
        font.setPointSize(14)
        self.resultsTable.setFont(font)

        self.resultsTable.setColumnWidth(1,150)
        self.resultsTable.setColumnWidth(2,150)
        self.resultsTable.setColumnWidth(3,200)

        for row in range(self.resultsTable.rowCount()):
            self.resultsTable.setRowHeight(row, 48)

        self.page3_layout.addWidget(self.resultsTable)

    def updateTable(self):
        try:
            self.tableWidget.deleteLater()
        except:
            pass

        self.tableWidget = None
        self.drawTable()
    
    def updateResultsTable(self, items):
        if items != 400 and items != "Error":
            try:
                self.resultsTable.deleteLater()
            except:
                pass

            try:
                self.goToButton.deleteLater()
            except:
                pass

            self.resultsTable = None
            self.drawResultsTable(items)

            self.goToButton = None
            self.goToButton = QPushButton("Go to IsThereAnyDeal.com")
            self.goToButton.setFont(QFont("Arial", 14))
            self.page3_layout.addWidget(self.goToButton)
        else:
            qm = QMessageBox()
            qm.setText("No results found.")
            qm.setIcon(QMessageBox.Information)
            qm.exec()

    def drawSearchWindow(self):
        print("todo")

    def switchTab(self, nextPage):
        # clear all elements and actions from toolbar
        self.toolbar.clear()

        for action in self.toolbars[nextPage]:
            # i dont know why but apparently this doesn't work unless you redefine spacer ...
            if action == self.spacer:
                spacer = QWidget()
                spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
                self.toolbar.addWidget(spacer)
            else:
                self.toolbar.addAction(action)

        # change the page
        self.stack.setCurrentIndex(nextPage)
   
# Pop out window asking for users to import game store paths
class StoreLocationWindow(QWidget):
    def __init__(self, steam, uplay, origin, epic):
        super().__init__()

        if not self.objectName():
            self.setObjectName(u"popout")

        self.setWindowTitle("Game Launcher Locations")
        self.setFixedSize(400,250)

        layout = QFormLayout()

        # This is kinda clunky but we can optimise it later
        steamLabel = QLabel("Steam Location")
        uplayLabel = QLabel("uPlay Location")
        originLabel = QLabel("Origin Location")
        epicLabel = QLabel("Epic Games Location")
        
        self.steamEdit = QLineEdit()
        self.steamEdit.setText(steam)
        
        self.uplayEdit = QLineEdit()
        self.uplayEdit.setText(uplay)
        
        self.originEdit = QLineEdit()
        self.originEdit.setText(origin)
        
        self.epicEdit = QLineEdit()
        self.epicEdit.setText(epic)

        # Placeholder text as internal help for Windows users. None for macOS or Linux as I do not know the necessary paths
        # and do not have the resources to test these systems yet.
        if platform.system() == "Windows":
            self.steamEdit.setPlaceholderText("e.g. C:/Program Files (x86)/Steam/steamapps")
            self.uplayEdit.setPlaceholderText("e.g. C:/Program Files (x86)/Ubisoft/Ubisoft Game Launcher")
            self.originEdit.setPlaceholderText("e.g. C:/ProgramData/Origin")
            self.epicEdit.setPlaceholderText("e.g. C:/ProgramData/Epic/UnrealEngineLauncher")

        self.steamButton = QPushButton("Browse...")
        self.uplayButton = QPushButton("Browse...")
        self.originButton = QPushButton("Browse...")
        self.epicButton = QPushButton("Browse...")
        self.saveButton = QPushButton("Save")

        layout.addRow(steamLabel)
        layout.addRow(self.steamButton, self.steamEdit)
        layout.addRow(uplayLabel)
        layout.addRow(self.uplayButton, self.uplayEdit)
        layout.addRow(originLabel)
        layout.addRow(self.originButton, self.originEdit)
        layout.addRow(epicLabel)
        layout.addRow(self.epicButton, self.epicEdit)
        layout.addRow(self.saveButton)

        self.setLayout(layout)
        self.show()

    def getLocation(self, store):
        if platform.system() == "Windows":
            directory = "C:/Program Files (x86)/"
        else:
            directory = "~/"

        if store == "Steam":
            thing = "steamapps"
        elif store == "Origin":
            thing = "Origin"
        elif store == "uPlay": # basically irrelevant because uplay uses registry, but will put this as a placeholder for now to determine whether or not to scan uplay.
            thing = "uPlay Launcher"
        else:
            thing = "Epic Games LauncherInstalled.dat directory"


        
        f = QFileDialog.getExistingDirectory(self, f"Select {thing} directory:", f"{directory}")

        # Set the directory and input it into the lineedit.
        if store == "Steam":
            self.steamEdit.setText(f)
        elif store == "uPlay":
            self.uplayEdit.setText(f)
        elif store == "Origin":
            self.originEdit.setText(f) # `C:\\ProgramData\\Origin` by default
        else:
            self.epicEdit.setText(f) #'C:\\ProgramData\\Epic\\UnrealEngineLauncher\\LauncherInstalled.dat'

# Popout loading screen. Quite possibly the worst way of doing it but it works and is easy. Might fix later idk.
class LoadingIcon(QWidget):
    def __init__(self, parent=None):
        super().__init__()
        self.setFixedSize(600, 400)
        self.setWindowTitle("Scanning...")

        self.label = QLabel(self)
        self.setFixedSize(600,338)
        self.movie = QMovie(f"{os.path.dirname(__file__)}/load.gif")
        self.label.setMovie(self.movie)
        self.movie.start()
        self.show()

# Icon class for table
class Icon(QLabel):
    def __init__(self, launcher, parent=None):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)

        self.dir = os.path.dirname(__file__)

        if launcher == "steam":
            self.setPixmap(QPixmap(f"{self.dir}/icons/steam.png", "png"))
        elif launcher == "uplay":
            self.setPixmap(QPixmap(f"{self.dir}/icons/uplay.png", "png"))
        elif launcher == "origin":
            self.setPixmap(QPixmap(f"{self.dir}/icons/origin.png", "png"))
        elif launcher == "epic":
            self.setPixmap(QPixmap(f"{self.dir}/icons/epic.png", "png"))
        elif launcher == "standalone":
            self.setPixmap(QPixmap(f"{self.dir}/icons/standalone.png", "png"))
        