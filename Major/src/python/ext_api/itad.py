import requests, json, webbrowser, os, random

# Refer to https://itad.docs.apiary.io/ to understand how the API works.
class itad():
    def __init__(self):
        
        self.dir = os.path.dirname(__file__)

        with open(f"{self.dir}/secrets.json", "r") as f:
            values = json.load(f)
            self.api_key = values["itad_api"]
            self.id = values["itad_id"]
            self.secret = values["itad"]

        self.code = ""
        self.token = ""

        self.url = "https://api.isthereanydeal.com"

    # to be honest, authorisation is not needed because I didn't end up getting to implement the wishlist stuff.
    # that being said, im keeping this because it took me way too long to get working.
    def requestAuthorize(self):
        random.seed() # need a random thingo as the state for the api request.
        self.state = str(random.random())[2:]
        url = self.url + "/oauth/authorize/?client_id=" + str(self.id) + "&response_type=code&state=" + self.state + "&scope=wait_read%20wait_write%20coll_read%20coll_write"
        webbrowser.open(url)

    # using the auth code, we get a token for the api.
    def getToken(self):
        url = self.url + "/oauth/token/"
        params = {
            "grant_type": "authorization_code",
            "code": self.code,
            "client_id": self.id,
            "client_secret": self.secret
        }
        response = requests.post(url, params)
        print(response.content)

        if response.status_code == 200:
            token = json.loads(json.dumps(response.json()))["access_token"]
            print(token)
            return token
        elif response.status_code == 400:
            print("Error 400")
        else:
            print(response.status_code)

    # the search function
    def search(self, query):
        url = self.url + "/v02/search/search/"
        params = {
            "key": self.api_key,
            "q": query
        }

        response = requests.post(url, params=params)

        if response.status_code == 200:
            # the api returns the stuff in json, but its formatted kinda ass
            r = json.loads(json.dumps(response.json()))["data"]["results"]
            u = json.loads(json.dumps(response.json()))["data"]["urls"]["search"]
            results = {}

            for i in range(len(r)):
                results[r[i]["plain"]] = {
                    "title": r[i]["title"],
                    "id": r[i]["id"]
                }
            
            return [self.getOverView(results), u]


        elif response.status_code == 400:
            print("Error 400")
            return 400

        else:
            print(response.status_code)
            return "Error"

    def getOverView(self, games):
        plainList = []

        for i in games:
            plainList.append(i)
        
        plains = ",".join(plainList)

        url = self.url + "/v01/game/overview"
        params = {
            "key": self.api_key,
            "plains": plains
        }

        response = requests.post(url, params=params)

        if response.status_code == 200:
            r = json.loads(json.dumps(response.json()))["data"]
            for i in r:
                for k in r[i]:
                    if k == "price":
                        if r[i][k] is not None:
                            games[i]["current"] = r[i][k]["price"]
                            games[i]["currentStore"] = r[i][k]["store"]
                        else:
                            games[i]["current"] = None
                            games[i]["currentStore"] = None

                    elif k == "lowest": 
                        if r[i][k] is not None:
                            games[i][k] = r[i][k]["price"]
                        else:
                            games[i][k] = None

            try:
                with open(f"{self.dir}/results.json", "w") as f:
                    json.dump(games, f)
            except:
                pass

            return games

        elif response.status_code == 400:
            print("Error 400")
            return 400
        else:
            print(response.status_code)
            return "Error"