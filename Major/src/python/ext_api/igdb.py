# This file is not used because I have no idea what the syntax or anything for this API is, and I didn't have time to study it.
# This remains because I put effort into connecting to this API.

import os, requests, json

class igdb():
    def __init__(self):

        # Don't know if this is the best way but here it is
        self.dir = os.path.dirname(__file__)

        with open(f"{self.dir}/secrets.json", "r") as f:
            values = json.load(f)
            self.id = values["igdb_id"]
            self.secret = values["igdb"]

        self.url = "https://api.igdb.com/v4"

        self.token = self.getToken()

        self.headers = {
            "Client-ID": self.id,
            "Authorization": f"Bearer {self.token}"
        }

    def getToken(self):
        params = {
            "client_id": self.id,
            "client_secret": self.secret,
            "grant_type": "client_credentials"
        }

        url = "https://id.twitch.tv/oauth2/token"

        try:
            response = requests.post(url, params)
            # print(response)
            if response.status_code == 200:
                token = response.json()["access_token"]
            else:
                print(f"Status code {response.status_code}")
                token = 0
            
            return token

        except Exception as e:
            print(e)
            return 0
    
    # def getArtwork(self, game):
    #     name = game.replace(" ","-").lower()
    #     url = self.url + "/artworks"
    #     headers = {
    #         "Client-ID": self.id,
    #         "Authorization": f"Bearer {self.token}"
    #     }

    #     try:
    #         response = requests.post(url, headers=headers)
    #     except:
    #         return False