inputting = True
sorting = True
array = []

while inputting:
    print(f"Current array: {str(array)}")
    response = input("Enter a number or type \"done\": ")

    if response == "done" or "":
        if len(array) > 0:
            inputting = False
        else:
            print("There must be at least 1 number in the array")
    else:
        try:
            array.append(int(response))
        except:
            print("Invalid Input")

while sorting:
    swapped = False
    for i in range(len(array)-1):
        if array[i] > array[i+1]:
            temp = array[i]
            array[i] = array[i+1]
            array[i+1] = temp
            swapped = True
    if not swapped:
        sorting = False

print(array)

input("Press enter to exit...")