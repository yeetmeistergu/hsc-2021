''' 
Code Explanation and Justification

3 Main Features:

1.) Scan for games from existing 3rd-party launchers, as well as importing custom standalone games
2.) Managing and launching games with support for custom parameters and launch options
3.) Integration with IsThereAnyDeal.com, allowing for searching and displaying games and prices

Since this app is intended to be a "functional" app, not a like a game, I needed a solution which was
capable, and designed for building a "functional" GUI. I could have used PyGame, but PyGame doesn't
handle things like text, buttons, tables, and filesystem stuff well. I had already used PyQt before,
and the Qt GUI framework had been used for similar purposes, most notably (imo) by the Dolphin Emulator,
which has a UI that is similar in design philosophy and function as my one (albeit, slightly better).

The general structure of my code, is that all the main functions and features occur in one window. The
MainWindow class is the main window of the application. The 2 main functions which my app is designed to
do, that being list/show/manage/launch games, and find game deals with ITAD are placed in the main window,
utilising a QStackedWidget to switch between the 2 pages. I utilised quite a few separate widget windows,
such as the StoreLocationWindow class, mainly because these offer the "secondary" functions, or subprocesses
of the main features, and these popout windows were easier to code, and separates everything so that the user
is not overwhelmed by too many things happening in one window. This modular approach seems to work, and doesn't
impact the user experience too much.

I've used a lot of QMessageBoxes to give feedback to the user if/when they make a booboo. The messages give the
user good feedback on what they should have done. Using native file dialogs as well is extremely handy, as it
makes the software more forgiving because the user will not have to manually input paths, which can result in
disaster.

Overall, I've tried to make my code as modular as possible for ease of maintenance, and I have tried to make
my UI as consistent, robust, and forgiving as I can, whilst also providing a good amount of flexibility and 
user feedback.

'''


# Import the necessary libraries
import os, sys, json, threading, time, platform, fnmatch, re, vdf, webbrowser

# Need Windows registery if on Windows for uPlay scanning
if platform.system() == "Windows":
    import winreg
    # print("Imported winreg")

# Importing more libraries, although only the required functions from them
from http.server import SimpleHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
from steamfiles import acf
from collections import OrderedDict
from nested_lookup import nested_lookup

# Import PySide6 modules
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtUiTools import *

# Import the custom modules
import ext_api
from ui import *

# Main window process
class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        # Identify what OS the system is running on
        self.platform = platform.system()

        # Set running directory to the directory the script/executable is
        self.dir = os.path.dirname(__file__)

        # Initialise IGDB API
        # self.igdb = ext_api.igdb() # sadly this didn't end up getting used, but keeping it here because APIs and it took me some effort.

        # SSL Issues when at school
        # if self.igdb.token == 0:
        #     qm = QMessageBox()
        #     qm.setText("Error obtaining IGDB token. Please check your SSL certificates. Features requiring IGDB API will not function this session.")
        #     qm.setIcon(QMessageBox.Warning)
        #     qm.exec()

        # Initialise IsThereAnyDeal.com API
        self.itad = ext_api.itad()

        # Set up launch URI for game stores
        self.launchers = {
            "steam": "steam://rungameid/",
            "uplay": "uplay://launch/",   # /0
            "origin": "origin://launchgame/",
            "epic": "com.epicgames.launcher://apps/" # +?action=launch&silent=true"
        }

        # Load UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Game Manager")

        # Some flags and variables
        self.scanning = False
        self.selectedRow = ""
        self.currentSearch = ""

        # Load settings from .json files
        self.loadConfig()
        self.loadGamePaths()

        # Ended up not using a menubar, this is leftover.
        # # Connect menu actions to functions
        # self.ui.menuAdd.triggered.connect(lambda:self.getGame())
        # self.ui.menuScan.triggered.connect(lambda:self.scanGames())
        # self.ui.menuPreferences.triggered.connect(lambda:self.showPreferences())

        # Connect the scan button if it exists.
        try:
            self.ui.page1_scanButton.clicked.connect(lambda:self.scanGames())
        except:
            pass

        # Connect the table if it exists:
        try:
            self.ui.tableWidget.clicked.connect(self.tableClicked)
            self.ui.tableWidget.doubleClicked.connect(self.tableDoubleClicked)
        except:
            pass

        # Connect the search button for the ITAD page
        self.ui.searchButton.clicked.connect(self.searchGames)

        # Connect the toolbar actions
        self.ui.addAction.triggered.connect(lambda:self.getGame())
        self.ui.scanAction.triggered.connect(lambda:self.scanGames())
        self.ui.playAction.triggered.connect(self.runGame)
        self.ui.manageAction.triggered.connect(self.manageGame)
        self.ui.connectITADAction.triggered.connect(lambda:self.connectITAD())
        self.ui.settingsAction.triggered.connect(lambda:self.showPreferences())
        

# ''' Begin Slots ''' #
    @Slot(QModelIndex)

    # Function for when the a game is selected.
    def tableClicked(self, index):
        self.selectedRow = self.ui.tableWidget.currentRow() 
        # i have to use currentrow, because for some reason index gives the wrong index when sorted. 
        # maybe im just dumb and reading the index wrong, but this works so...

    # when a game is double clicked, launch it
    def tableDoubleClicked(self, index):
        row = self.ui.tableWidget.currentRow() # grab current row
        g = self.ui.tableWidget.item(row, 1).text() # g is the game name
        game = self.games[g] # game gets the dictionary associated with the game name
        self.launchGame(game)

    # on the find deals page, if a game is double clicked, goes to that game's page
    def goToITAD(self, index):
        row = self.ui.resultsTable.currentRow()
        g = self.ui.resultsTable.item(row, 0).text()
        # this bit is super backwards because i realised i set the plain name for the game
        # as the dictionary key, but the table doesnt store or display this. might make this
        # slower but it works and saves a lot of hassle.
        for i in self.results[0]:
            if self.results[0][i]["title"] == g:
                url = "https://isthereanydeal.com/game/" + i 
                webbrowser.open(url)

# ''' End Slots ''' #

# ''' Begin Standard Algorithm ''' #

    # Not yet used, but the general gist is here.
    def bubbleSort(self, items):
        sorting = True
        while sorting:
            swapped = False
            for i in range(len(items)-1):
                if items[i]["price"] > items[i+1]["price"]:
                    temp = items[i]["price"]
                    items[i]["price"] = items[i+1]["price"]
                    items[i+1]["price"] = temp
                    swapped = True
            if not swapped:
                sorting = False

# ''' End Standard Algorithm ''' #

# ''' Begin Launch Games ''' #
    def runGame(self):
        try:
            g = self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 1).text()
            game = self.games[g]
            self.launchGame(game)
        except:
            qm = QMessageBox()
            qm.setText("Please select a game to run.")
            qm.setIcon(QMessageBox.Warning)
            qm.exec()   

    def launchGame(self, game):
        try:
            cmd = game["command"]
            id = str(game["id"])

      
            if game["launcher"] == "steam":
                if self.options["silentLaunch"]:
                    os.startfile(cmd + id + " -silent")
                else:
                    os.startfile(cmd + id)
            elif game["launcher"] == "uplay":
                if self.options["silentLaunch"]:
                    os.startfile(cmd + id + "/0")
                else:
                    os.startfile(cmd + id)

            elif game["launcher"] == "origin":
                os.startfile(cmd + id)

            elif game["launcher"] == "epic":
                if self.options["silentLaunch"]:
                    os.startfile(cmd + id + "?action=launch&silent=true")
                else:
                    os.startfile(cmd + id + "?action=launch")
            
            else:
                os.startfile(cmd)
        
        except:
            qm = QMessageBox()
            qm.setText("Error launching game. For standalone games, please check the executable and/or launch commands are correct.")
            qm.setIcon(QMessageBox.Warning)
            qm.exec()

# ''' End Launch Games ''' #

# ''' Begin Manage Games ''' #

    def manageGame(self):
        # identify what game is selected, and then open the manage popout
        try:
            game = self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 1).text()
            # self.switchTab(1)
            self.manageMenu = ManageWindow(game)
            self.manageMenu.deleteButton.clicked.connect(lambda:self.removeGame(game))
            self.manageMenu.saveButton.clicked.connect(lambda:self.saveChanges(game))

        except:
            qm = QMessageBox()
            qm.setText("Please select a game to manage.")
            qm.setIcon(QMessageBox.Warning)
            qm.exec() 

    def saveChanges(self, game):
        # save the changes made.
        path = self.manageMenu.locationEdit.text()
        command = self.manageMenu.launchEdit.text().replace("&&path", path) 
        # i have included &&path and &&file as variables for custom commands 
        # to make it easier for the user. This will be documented in the manual.

        self.games[game]["path"] = path
        self.games[game]["command"] = command
        self.games[game]["arguments"] = self.manageMenu.parameterEdit.text()
        self.saveGames()

        self.manageMenu.close()

    def removeGame(self, game):
        # user confirmation is important. makes forgiving code.
        qm = QMessageBox()
        qm.setText("Are you sure you want to remove this game from your library?")
        qm.setIcon(QMessageBox.Information)
        yes = QPushButton("Yes")
        no = QPushButton("No")
        qm.addButton(yes, QMessageBox.YesRole)
        qm.addButton(no, QMessageBox.NoRole)
        qm.exec()

         # User response
        res = qm.clickedButton().text() 

        if res == "Yes":
            del self.games[game]
            self.saveGames()
            self.manageMenu.close()
            self.ui.updateTable()

# ''' End Manage Games ''' #

# ''' Begin Search Games ''' #

    def searchGames(self):
        query = self.ui.searchBar.text()
        if query != "": # check if the user actually typed anything
            self.results = self.itad.search(query) # get the results of the query. it returns the dictionary of results, and the url as a list. [dict, url]
            self.ui.updateResultsTable(self.results[0]) # update the results table with the results
            # this is here because in the case of an invalid response, the button doesn't get created, and there is no url
            try:
                self.ui.goToButton.clicked.connect(lambda:webbrowser.open(self.results[1]))
            except:
                pass
            #connect the table to the doubleclikc funciton
            self.ui.resultsTable.doubleClicked.connect(self.goToITAD)
            
            # remind the user they are dumb
        else:
            qm = QMessageBox()
            qm.setText("Please enter a game to search for.")
            qm.setIcon(QMessageBox.Warning)
            qm.exec()

# ''' End Search Games ''' #

# ''' Begin Toggles ''' #

    # some options
    # launch the game silently using the launchers' silentlaunch options
    def toggleSilent(self):
        print(self.options["silentLaunch"])
        if self.options["silentLaunch"] == True:
            self.options["silentLaunch"] = False
            print(self.options["silentLaunch"])
        else:
            self.options["silentLaunch"] = True
        self.saveConfig()
        print(self.options["silentLaunch"])

    # this didnt end up being implemented
    # close the third party launcher on game exit
    def toggleClose(self):
        if self.options["closeOnExit"] == True:
            self.options["closeOnExit"] = False
        else:
            self.options["closeOnExit"] = True
        self.saveConfig()

# ''' End Toggles ''' #

# ''' File/Config Loading ''' #
    def loadGamePaths(self):
        # try loading the config file. if there is a permission error, cry
        try:
            with open(f"{self.dir}/games.json") as games:
                self.games = json.load(games)

        except PermissionError:
            qm = QMessageBox()
            qm.setText("Error opening game paths.")
            qm.setIcon(QMessageBox.Warning)
            qm.exec()
            self.games = {}
        
        # prompt the user that no games have been stored, offering to enter library locations
        except:
            qm = QMessageBox()
            qm.setText("No games have been found in your library! Would you like to enter your game library locations?")
            qm.setIcon(QMessageBox.Information)
            yes = QPushButton("Yes")
            no = QPushButton("No")
            qm.addButton(yes, QMessageBox.YesRole)
            qm.addButton(no, QMessageBox.NoRole)
            qm.exec()
            self.games = {}

            # User response
            res = qm.clickedButton().text() 

            if res == "Yes":
                self.show()
                self.showLocationPopout()
                
            else:
                # Show UI
                self.show()

    # load the config file
    def loadConfig(self):
        try:
            # try to parse the config options
            with open(f"{self.dir}/config.json") as cfg:
                config = json.load(cfg)

                self.launcherDirectories = config["stores"]
                self.options = config["options"]
                self.itad.token = config["token"]
                
                self.hasConfig = True
                print("Successfully loaded config")

        # if perm error, dont go ahead.
        except PermissionError:
            qm = QMessageBox()
            qm.setWindowTitle("Error")
            qm.setText("Error accessing config file. Please check file permissions.")
            qm.setIcon(QMessageBox.Critical)
            qm.setStandardButtons(QMessageBox.Ok)
            qm.exec()
            exit()

        # create a new config file if old one invalid or not found.
        except:
            with open(f"{self.dir}/config.json", "w") as cfg:
                config = {
                    "stores": {
                        "steam": "",
                        "uplay": "",
                        "origin": "",
                        "epic": ""
                    },
                    "options": {
                        "silentLaunch": True,
                        "closeOnExit": True,
                        "showSteam": True,
                        "showOrigin": True,
                        "showUplay": True,
                        "showEpic": True,
                        "showOther": True
                    },
                    "token": ""
                }
                json.dump(config, cfg)

            self.launcherDirectories = {
                "steam": "",
                "uplay": "",
                "origin": "",
                "epic": ""
            }

            self.options = {
                "silentLaunch": True,
                "closeOnExit": True,
                "showSteam": True,
                "showOrigin": True,
                "showUplay": True,
                "showEpic": True,
                "showOther": True
            }

    # write the config stuff to file
    def saveConfig(self):
        try:
            print(self.options)
            with open(f"{self.dir}/config.json", "w") as cfg:
                config = {
                    "stores": {
                        "steam": self.launcherDirectories["steam"],
                        "uplay": self.launcherDirectories["uplay"],
                        "origin": self.launcherDirectories["origin"],
                        "epic": self.launcherDirectories["epic"]
                    },
                    "options": self.options,
                    "token": self.itad.token
                }

                json.dump(config, cfg)
            
            print("Config saved successfully")
        
        #cry 
        except:
            qm = QMessageBox()
            qm.setWindowTitle("Error")
            qm.setText("An error has occurred whilst trying to save the config. Please check your file permissions and try again.")
            qm.setIcon(QMessageBox.Critical)
            qm.setStandardButtons(QMessageBox.Ok)
            qm.exec()

    # save the locations that were entered
    def saveLocations(self):
        try:
            self.launcherDirectories["steam"] = self.popout.steamEdit.text()
            self.launcherDirectories["uplay"] = self.popout.uplayEdit.text()
            self.launcherDirectories["origin"] = self.popout.originEdit.text()
            self.launcherDirectories["epic"] = self.popout.epicEdit.text()
            self.popout.close()

        # this shouldn't happen
        except:
            qm = QMessageBox()
            qm.setWindowTitle("Error")
            qm.setText("Error accessing setting launcher locations. Please check the directories inputted are valid.")
            qm.setIcon(QMessageBox.Critical)
            qm.setStandardButtons(QMessageBox.Ok)
            qm.exec()
        
        self.saveConfig()

    # save the games list.
    def saveGames(self):
        try:
            with open(f"{self.dir}/games.json", "w") as games:
                json.dump(self.games, games)
            
            print("Games saved successfully")

        except:
            qm = QMessageBox()
            qm.setWindowTitle("Error")
            qm.setText("An error has occurred saving the list of scanned games. Please check file permissions and try again.")
            qm.setIcon(QMessageBox.Critical)
            qm.setStandardButtons(QMessageBox.Ok)
            qm.exec()

# ''' End File/Config Loading ''' #

# ''' Begin Scanning Functions ''' #

    def loadingScreen(self):
        self.loadingIcon = LoadingIcon()

    ''' Might have to break this module up. We'll see if I have time.'''

    # Some code derived from https://github.com/sakshatshinde/Plei/blob/master/src/gameOps.py
    def scanGames(self):
        if not self.scanning: # make sure the user doesnt spam the scan button which can become a nightmare.
            self.scanning = True
            thread = threading.Thread(target = self.loadingScreen)
            thread.daemon = True
            thread.run()

            for i in self.launcherDirectories:
                if self.launcherDirectories[i] != "":
                    if i == "steam":
                        try:
                            directory = f"{self.launcherDirectories[i]}"
                            libraries = [directory]
                            files = os.listdir(directory)

                            for file in files:
                                if file == "libraryfolders.vdf":
                                    with open(f"{directory}/libraryfolders.vdf") as f:
                                        v = vdf.load(f)
                                        k = v["LibraryFolders"] # this is how libraryfolders.vdf is formatted as of 01/09/2021
                                        for key in k:
                                            try:
                                                int(key)
                                                libraries.append(k[key])

                                            except:
                                                pass
                                    break
                            
                            for library in libraries:
                                files = os.listdir(library)
                                ext = "*.acf"

                                for file in files:
                                    if fnmatch.fnmatch(file, ext):
                                        try:
                                            with open(f"{library}/{file}") as acfFile:
                                                data = acf.load(acfFile, wrapper=OrderedDict)
                                                name = nested_lookup("name", data)
                                                appid = nested_lookup("appid", data)
                                                installPath = nested_lookup("installdir", data)
                                                if name[0] not in self.games:
                                                    self.games[name[0]] = {
                                                        "id": appid[0],
                                                        "path": f"{library}/common/{installPath[0]}",
                                                        "launcher": "steam",
                                                        "command": self.launchers["steam"],
                                                        "arguments": ""
                                                    }
                                        except:
                                            pass
                        except:
                            qm = QMessageBox()
                            qm.setWindowTitle("Error")
                            qm.setText("An error has occurred whilst scanning for games from Steam. Please check that you have set the steamapps directory correctly.")
                            qm.setIcon(QMessageBox.Critical)
                            qm.setStandardButtons(QMessageBox.Ok)
                            qm.exec()

                    elif self.platform == "Windows":
                        if i == "origin": # this part is mostly fully borrowed from the project referenced above
                            try:
                                ext = "*.mfst"
                                gameName, gameID = [], []
                                # root is not used but thats just how its formatted
                                for root, dirs, files in os.walk(f"{self.launcherDirectories[i]}/LocalContent"):      # C:\\ProgramData\\Origin\\LocalContent
                                    for dir in dirs:
                                        gameName.append(dir)
                                    for fileName in files:
                                        if fnmatch.fnmatch(fileName, ext):
                                            fileName = fileName[:-5] 
                                            gameID.append(fileName)
                                
                                final = dict(zip(gameName, gameID))
        
                                for game, gameID in final.items():
                                    if game not in self.games:
                                        self.games[game] = {
                                            "id": gameID,
                                            "path": "",
                                            "launcher": "origin",
                                            "command": self.launchers["origin"],
                                            "arguments": ""
                                        } 
                            except:
                                qm = QMessageBox()
                                qm.setWindowTitle("Error")
                                qm.setText("An error has occurred whilst scanning for games from Origin. Please check that you have set the Origin directory correctly (\"C:/ProgramData/Origin\" by default).")
                                qm.setIcon(QMessageBox.Critical)
                                qm.setStandardButtons(QMessageBox.Ok)
                                qm.exec()

                        # Unable to test uPlay and Epic Games function because I don't own any uplay or egs games :\
                        elif i == "uplay": # this bit is funky because apparently has to be done through windows registry which is dumb.
                            try:
                                baseReg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
                                subKey = winreg.OpenKey(baseReg, "SOFTWARE\\WOW6432Node\\Ubisoft\\Launcher\\Installs\\")

                                for _ in range(50) :
                                    try :
                                        gameID = winreg.EnumKey(subKey, _)

                                        gameKey = winreg.OpenKey(baseReg, "SOFTWARE\\WOW6432Node\\Ubisoft\\Launcher\\Installs\\" + gameID + "\\")
                                        name = winreg.EnumValue(gameKey, 1)     # Name is a list of keys returned by the func
                                        path = name[1]                              # The pos 1 contains Game Directory
                                        path = os.path.dirname(path)
                                        game = os.path.basename(path)           # The path name is actually the ID for the game
                                        
                                        if game not in self.games:
                                            self.games[game] = {
                                                "id": str(gameID),
                                                "path": path,
                                                "launcher": "uplay",
                                                "command": self.launchers["uplay"],
                                                "arguments": ""
                                            }

                                    except :
                                        pass

                                winreg.CloseKey(baseReg)
                            except:
                                qm = QMessageBox()
                                qm.setWindowTitle("Error")
                                qm.setText("An error has occurred whilst scanning for games from uPlay. Please check that this application has sufficient permissions to access the registry.")
                                qm.setIcon(QMessageBox.Critical)
                                qm.setStandardButtons(QMessageBox.Ok)
                                qm.exec()

                        elif i == "epic": #'C:\\ProgramData\\Epic\\UnrealEngineLauncher\\LauncherInstalled.dat'
                            try:
                                with open(f"{self.launcherDirectories[i]}/LauncherInstalled.dat") as f:
                                    n = 0
                                    data = json.load(f)
                                    gameID = nested_lookup('AppName', data)
                                    gameName = []
                                    dirPath = nested_lookup('InstallLocation', data) # returns a directory path
                                    for name in dirPath:
                                        name = re.search(r"(\\)(\w+)($)", name) # Regex for finding the game's name from the directory path
                                        gameName.append(name.group().strip('\\'))
                                
                                final = dict(zip(gameName, gameID)) # combining the game's actual name with the codename
                                
                                for game, gameID in final.items():        # Adding game details to the master list
                                    if game not in self.games:    
                                        self.games[game] = {
                                            "id": gameID,
                                            "path": dirPath[n], # i am really proud of this line :) i feel like a freakin genius for figuring this out
                                            "launcher": "epic",
                                            "command": self.launchers["epic"],
                                            "arguments": ""
                                        }
                                    n += 1
                            except:
                                qm = QMessageBox()
                                qm.setWindowTitle("Error")
                                qm.setText("An error has occurred whilst scanning for games from Epic Games. Please check that you have set the directory for LauncherInstalled.dat correctly.")
                                qm.setIcon(QMessageBox.Critical)
                                qm.setStandardButtons(QMessageBox.Ok)
                                qm.exec()

                self.saveGames()
                self.ui.updateTable()
            # shut down the laoding screen    
            self.loadingIcon.deleteLater()
            self.loadingIcon = None

            # get rid of the big scan button if it exists
            try:
                open(f"{self.dir}/games.json")
                self.ui.page1_layout.removeWidget(self.ui.page1_scanButton)
                self.ui.page1_scanButton.deleteLater()
                self.ui.page1_scanButton = None
            except:
                pass

            self.scanning = False

    # manually adding game
    def getGame(self):
        f = QFileDialog.getOpenFileName(self, f"Select game executable:", f"{self.dir}")
        if f[0] != "":
            name = os.path.basename(f[0]).split(".")[0].capitalize()
            path = os.path.dirname(f[0])
            self.getStandaloneCommand(name, path, f[0])
    
    def getStandaloneCommand(self, name, path, command):
        self.cmdWindow = LaunchCommandsWindow()
        self.cmdWindow.saveButton.clicked.connect(lambda:self.cmdWindow.getCommand(name, path, command))

# ''' End Scanning Functions ''' #

# ''' Begin External Windows '''#
    def showLocationPopout(self):
        self.popout = StoreLocationWindow(self.launcherDirectories["steam"], self.launcherDirectories["uplay"], self.launcherDirectories["origin"], self.launcherDirectories["epic"])
        self.popout.steamButton.clicked.connect(lambda:self.popout.getLocation("Steam"))
        self.popout.uplayButton.clicked.connect(lambda:self.popout.getLocation("uPlay"))
        self.popout.originButton.clicked.connect(lambda:self.popout.getLocation("Origin"))
        self.popout.epicButton.clicked.connect(lambda:self.popout.getLocation("Epic Games"))
        self.popout.saveButton.clicked.connect(lambda:self.saveLocations())

    def showPreferences(self):
        self.preferencesWindow = PreferencesWindow(self.options["silentLaunch"], self.options["closeOnExit"])

# ''' End External Windows ''' #

# ''' Begin API Handling ''' #

    def connectITAD(self):
        if self.itad.token == "":
            self.startHTTPServer()
            self.itad.requestAuthorize()
        else:
            qm = QMessageBox()
            qm.setWindowTitle("Connect ITAD")
            qm.setText("ITAD Already Connected!")
            qm.setIcon(QMessageBox.Information)
            qm.exec()

# ''' End API Handling ''' #

# ''' HTTP Server ''' #
    def startHTTPServer(self):
        os.chdir(self.dir)
        self.httpd = HTTPServer(('', 80), RequestHandler)
        thread = threading.Thread(target = self.httpd.serve_forever)
        thread.daemon = True
        thread.start()

    def stopHTTPServer(self):
        time.sleep(3)
        self.httpd.shutdown()
# ''' End HTTP Server ''' #

# has to be in this file instead of UI just because values need changing.
class PreferencesWindow(QWidget):
    def __init__(self, silent, close):
        super().__init__()
        
        if not self.objectName:
            self.setObjectName(u"pref")

        self.setFixedSize(300,100)
        self.setWindowTitle("Preferences")

        layout = QFormLayout()

        self.silentLabel = QLabel("Silent Launch")
        self.silentLabel.setAlignment(Qt.AlignCenter)
        self.silentCheck = QCheckBox()
        if silent:
            self.silentCheck.setChecked(True)
        self.silentCheck.toggled.connect(window.toggleSilent)

        # ----------------------------------------
        # May not have time to properly implement
        # ----------------------------------------
        # self.closeLabel = QLabel("Close 3rd-party launcher on game exit")
        # self.closeLabel.setAlignment(Qt.AlignCenter)
        # self.closeCheck = QCheckBox()
        # if close:
        #     self.closeCheck.setChecked(True)
        # self.closeCheck.toggled.connect(window.toggleClose)

        self.editPathButton = QPushButton("Edit game launcher paths...")
        self.editPathButton.clicked.connect(window.showLocationPopout)

        layout.addRow(self.silentCheck, self.silentLabel)
        # layout.addRow(self.closeCheck, self.closeLabel)
        layout.addRow(self.editPathButton)

        self.setLayout(layout)
        self.show()

# popup for custom launch command for standalone game
class LaunchCommandsWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.setFixedSize(300,100)
        self.setWindowTitle("Launch Commands")

        layout = QFormLayout()

        label = QLabel("Enter custom launch command (leave blank if none)")
        self.line = QLineEdit()
        self.saveButton = QPushButton("Save")

        layout.addRow(label)
        layout.addRow(self.line)
        layout.addRow(self.saveButton)

        self.setLayout(layout)
        self.show()
    
    def getCommand(self, name, path, command):
        if self.line == "":
            window.games[name] = {
                "id": "-",
                "path": path,
                "launcher": "standalone",
                "command": command,
                "arguments": ""
            }
        else:
            c = self.line.text().replace("&&file", os.path.basename(command))
            cmd = c.replace("&&path", path)
            window.games[name] = {
                "id": "-",
                "path": path,
                "launcher": "standalone",
                "command": cmd,
                "arguments": ""
            }
        window.saveGames()
        window.ui.updateTable()

        self.close()

class ManageWindow(QWidget):
    def __init__(self, game):
        super().__init__()

        self.setFixedSize(400, 300)
        self.setWindowTitle("Manage Game")

        layout = QFormLayout()

        title = QLabel(f"{game}")
        font = title.font()
        font.setPointSize(24)
        title.setFont(font)
        title.setAlignment(Qt.AlignCenter)

        locationLabel = QLabel("Game Location")
        self.locationButton = QPushButton("Open location")
        self.locationEdit = QLineEdit(window.games[game]["path"])
        self.locationButton.clicked.connect(lambda:os.startfile(window.games[game]["path"]))

        launchLabel = QLabel("Launch Command")
        self.launchEdit = QLineEdit(window.games[game]["command"])

        parameterLabel = QLabel("Launch Parameters")
        self.parameterEdit = QLineEdit(window.games[game]["arguments"])

        self.deleteButton = QPushButton("Remove Game from Library")

        self.saveButton = QPushButton("Save Settings")

        layout.addRow(title)
        layout.addRow(locationLabel)
        layout.addRow(self.locationEdit)
        layout.addRow(self.locationButton)
        layout.addRow(launchLabel)
        layout.addRow(self.launchEdit)
        layout.addRow(parameterLabel)
        layout.addRow(self.parameterEdit)
        layout.addRow(self.deleteButton)
        layout.addRow(self.saveButton)

        self.setLayout(layout)

        self.show()

# Request handler for the simple HTTP server to handle the OAuth redirect.
# This may not be the best method, but it works for now.
class RequestHandler(SimpleHTTPRequestHandler):
    # What the HTTP server sends to the request client when it receives a GET request
    def do_GET(self):
        # Send success response code
        self.send_response(200)
        # HTTP headers
        self.send_header('Content-type','text/html')
        self.end_headers()
        query = urlparse(self.path).query
        # Attempt to parse the token from URL response
        try:
            response = dict(q.split("=") for q in query.split("&"))
            window.itad.code = response["code"]
            window.itad.token = window.itad.getToken()
            self.wfile.write(bytes("<h1> Success! You may now close this window. </h1>", "utf-8"))
            window.saveConfig()
        # If user doesn't authenticate, or something wrong happens
        except Exception as e:
            self.wfile.write(bytes("<h1> Error Authenticating. You may have cancelled the request, or something may have gone wrong. Please try again later. </h1>", "utf-8"))
            print(e)
        # Shutdown HTTP server afterwards
        finally:
            thread = threading.Thread(target = window.stopHTTPServer)
            thread.daemon = True
            thread.start()

if __name__ == "__main__":
    app = QApplication([])
    
    window = MainWindow()
    window.show()

    sys.exit(app.exec())